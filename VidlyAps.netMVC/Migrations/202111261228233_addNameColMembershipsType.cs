namespace VidlyAps.netMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addNameColMembershipsType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MembershipTypes", "Name", c => c.String(nullable:true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MembershipTypes", "Name");
        }
    }
}
