﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VidlyAps.netMVC.Startup))]
namespace VidlyAps.netMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
