﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VidlyAps.netMVC.Models;

namespace VidlyAps.netMVC.Controllers
{
    public class MoviesController : Controller
    {
        private ApplicationDbContext db;

        public MoviesController()
        {
            db = new ApplicationDbContext();
        }
        // GET: Movies
        public ActionResult Index()
        {
            var movies = db.Movies.Include(m=>m.Genre).ToList();
            return View(movies);
        }
        // GET: Customer/Details/5
        public ActionResult Details(int id)
        {
            var movies = db.Movies.Include(m => m.Genre).SingleOrDefault(c => c.Id == id);
            if (movies == null)
                return HttpNotFound();
            return View(movies);
        }

    }
}