﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VidlyAps.netMVC.Models
{
    public class MembershipType
    {
        public int Id { get; set; }
        public short SignUpfee { get; set; }
        public byte DurationInMonth { get; set; }
        public byte DiscountRate { get; set; }
        public string Name { get; set; }
    }
}